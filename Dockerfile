FROM gcr.io/distroless/java
LABEL "org.opencontainers.image.vendor"="1800Flowers.com"

COPY /target/csi-ivr-services-*.jar app.jar
CMD ["app.jar"]