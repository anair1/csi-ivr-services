package com.csi.ivr.services;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.csi.ivr.services.controller.OpenCloseCSIController;
import com.csi.ivr.services.model.OpenCloseCSIBean;
import com.csi.ivr.services.util.IVRProducerUtil;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;


@RunWith(MockitoJUnitRunner.class)
public class OpenCloseCSIControllerTests {
	
	@InjectMocks
	private OpenCloseCSIController openCloseCSIController;
	
	@Mock
	IVRProducerUtil ivrProducerUtil;
	
	
	@Test
	public void publishMessageTest_ValidRequest() throws JsonParseException, JsonMappingException, IOException {
		
		
		
		OpenCloseCSIBean bean = new OpenCloseCSIBean();
		ResponseEntity<Void> publishMessage = openCloseCSIController.openCloseCSI(bean);
		assertEquals(HttpStatus.OK, publishMessage.getStatusCode());
	}

}
