package com.csi.ivr.services.model;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;

public class OpenCloseCSIBeanTests {
	
	
	@Test
	public void test() {
		
		OpenCloseCSIBean bean = new OpenCloseCSIBean();
		bean.setOdrNumber(194861033);
		bean.setVersion(1);
		bean.setLeaveOpenCloseCSI("N");
		bean.setGiftCer("N");
		bean.setGcAmt(0.00);
		bean.setPartialCredit("N");
		bean.setpCreditAmt(0.00);
		bean.setFullCredit("N");
		bean.setfCreditAmt(0.00);
		bean.setMsgFlorist("N");
		bean.setMisc("Y");
		bean.setRedelApology("N");
		bean.setReptFlag("N");
		bean.setCallerCode("C");
		bean.setContactType("I");
		bean.setProblemCode("S");
		bean.setDetailCode("SC");
		bean.setReasonCode("SSC");
		bean.setResCode("1");
		bean.setSuggestResCode("M");
		bean.setStatusCode("X");
		bean.setFaultCode("C");
		bean.setErrorCode("I");
		bean.setCallBackRequiredFlag("N");
		bean.setTiPhoneNo("");
		bean.setCallBackTime("");
		bean.setTiEmail("test@gmail.com");
		bean.setStartTime("2020-04-10T12:06:02.151Z");
		bean.setUser("7545");
		bean.setResolutionBrand("18F");
		bean.setManageridOverride("");
		bean.setReasonForOverride("");
		bean.setSuggestedResId(301);
		
		assertNotNull(bean.getOdrNumber());
		assertNotNull(bean.getVersion());
		assertNotNull(bean.getLeaveOpenCloseCSI());
		assertNotNull(bean.getGiftCer());
		assertNotNull(bean.getGcAmt());
		assertNotNull(bean.getPartialCredit());
		assertNotNull(bean.getpCreditAmt());
		assertNotNull(bean.getFullCredit());
		assertNotNull(bean.getfCreditAmt());
		assertNotNull(bean.getMsgFlorist());
		assertNotNull(bean.getMisc());
		assertNotNull(bean.getRedelApology());
		assertNotNull(bean.getReptFlag());
		assertNotNull(bean.getCallerCode());
		assertNotNull(bean.getContactType());
		assertNotNull(bean.getProblemCode());
		assertNotNull(bean.getReasonCode());
		assertNotNull(bean.getResCode());
		assertNotNull(bean.getSuggestResCode());
		assertNotNull(bean.getStatusCode());
		assertNotNull(bean.getFaultCode());
		assertNotNull(bean.getErrorCode());
		assertNotNull(bean.getCallBackRequiredFlag());
		assertNotNull(bean.getTiPhoneNo());
		assertNotNull(bean.getCallBackTime());
		assertNotNull(bean.getTiEmail());
		assertNotNull(bean.getStartTime());
		assertNotNull(bean.getUser());
		assertNotNull(bean.getResolutionBrand());
		assertNotNull(bean.getManageridOverride());
		assertNotNull(bean.getReasonForOverride());
		assertNotNull(bean.getSuggestedResId());
		
		
		
	}

}
