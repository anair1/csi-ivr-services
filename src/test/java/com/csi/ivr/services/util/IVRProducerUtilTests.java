package com.csi.ivr.services.util;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.csi.ivr.services.model.OpenCloseCSIBean;


@RunWith(MockitoJUnitRunner.class)
public class IVRProducerUtilTests {
	
	@InjectMocks
	private IVRProducerUtil ivrProducerUtil;
	
	@Mock
	private IVRMessageProducer ivrMessageProducer;
	
	
	
	
	@Test
	public void publishMessageTest() {
		OpenCloseCSIBean bean= new OpenCloseCSIBean();
		ivrProducerUtil.publishMessage(bean);
		assertNotNull(ivrMessageProducer);
	}
	

}
