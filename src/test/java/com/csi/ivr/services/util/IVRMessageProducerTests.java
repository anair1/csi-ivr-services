package com.csi.ivr.services.util;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.core.env.Environment;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.util.concurrent.SettableListenableFuture;

import com.csi.ivr.services.model.OpenCloseCSIBean;


@RunWith(MockitoJUnitRunner.class)
public class IVRMessageProducerTests {
	
	@InjectMocks
	IVRMessageProducer ivrMessageProducer;
	
	@Mock
	private KafkaTemplate<String, OpenCloseCSIBean> kafkaTemplate;
	
	@Mock
	private IVRMessageHandler ivrMessageHandler;
	
	@Mock
	Environment environment;
	
	@Test
	public void publishMessageToTopicTest() {

		
		Mockito.when(kafkaTemplate.send(Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(new SettableListenableFuture<>());
		OpenCloseCSIBean bean=new OpenCloseCSIBean();
		bean.setOdrNumber(1234);
		ivrMessageProducer.publishMessageToTopic(bean);

	}
	
	

}
