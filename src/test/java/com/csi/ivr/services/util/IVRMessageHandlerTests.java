package com.csi.ivr.services.util;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.kafka.support.SendResult;

import com.csi.ivr.services.model.OpenCloseCSIBean;

@RunWith(MockitoJUnitRunner.class)
public class IVRMessageHandlerTests {
	
	@InjectMocks
	private IVRMessageHandler IVRMessageHandler;
	
	@Test
	public void onSuccessTest() {
		IVRMessageHandler.onSuccess(new SendResult<String, OpenCloseCSIBean>(null, null));
	}
	
	@Test
	public void onFailureTest() {
		IVRMessageHandler.onFailure(new Exception("Its an Exception"));
	}

}
