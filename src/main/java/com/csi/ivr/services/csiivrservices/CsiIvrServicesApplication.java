package com.csi.ivr.services.csiivrservices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CsiIvrServicesApplication {

	public static void main(String[] args) {
		SpringApplication.run(CsiIvrServicesApplication.class, args);
	}

}
