package com.csi.ivr.services.util;

import org.springframework.boot.actuate.health.HealthIndicator;

import com.csi.ivr.services.model.OpenCloseCSIBean;



public interface IVRCommonService extends HealthIndicator {
	
	public void publishMessage(OpenCloseCSIBean bean);

}
