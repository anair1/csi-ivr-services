package com.csi.ivr.services.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;

import com.csi.ivr.services.model.OpenCloseCSIBean;



@Service
public class IVRMessageProducer {
	
Logger logger = LoggerFactory.getLogger(IVRMessageProducer.class);
	
	@Autowired
	private KafkaTemplate<String, OpenCloseCSIBean> kafkaTemplate;
	
	@Autowired
	private IVRMessageHandler ivrMessageHandler;
	
	@Autowired
	Environment environment;
	
	public void publishMessageToTopic(OpenCloseCSIBean bean) {

		String producerTopicName = environment.getProperty("producerTopicName");
		
		logger.info("[Order Number] publishMessage : Publishing message for order number : "+bean.getOdrNumber()+" to topic : "+producerTopicName);
		
		ListenableFuture<SendResult<String,OpenCloseCSIBean>> kafkaListener = kafkaTemplate.send(producerTopicName, bean.getOdrNumber().toString(), bean);
		
		logger.trace("[Order Number] publishMessage : Publishing message completed for order number : "+bean.getOdrNumber());
		
		kafkaListener.addCallback(ivrMessageHandler);

	}

}
