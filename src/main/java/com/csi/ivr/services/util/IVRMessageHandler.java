package com.csi.ivr.services.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFutureCallback;

import com.csi.ivr.services.model.OpenCloseCSIBean;



@Component
public class IVRMessageHandler implements ListenableFutureCallback<SendResult<String, OpenCloseCSIBean>> {

	Logger logger = LoggerFactory.getLogger(IVRMessageHandler.class);
	
	@Override
	public void onSuccess(SendResult<String, OpenCloseCSIBean> result) {
		
		logger.info("sent message with offset :: '{}' ",
				null == result.getRecordMetadata() ? 0: result.getRecordMetadata().offset());
	}

	@Override
	public void onFailure(Throwable ex) {
		logger.error("Exception while publishing message ::  ", ex);
	}

}
