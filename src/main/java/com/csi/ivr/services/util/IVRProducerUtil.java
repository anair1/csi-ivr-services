package com.csi.ivr.services.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.Health;
import org.springframework.stereotype.Service;

import com.csi.ivr.services.model.OpenCloseCSIBean;

@Service
public class IVRProducerUtil implements IVRCommonService {
	
	@Autowired
	IVRMessageProducer ivrMessageProducer;
	
	private static final Logger logger = LoggerFactory.getLogger(IVRProducerUtil.class);

	@Override
	public Health health() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void publishMessage(OpenCloseCSIBean bean) {
		logger.trace("[Orde Number] publishMessage : Service execution started for order Number : "+bean.getOdrNumber());

		try {
			ivrMessageProducer.publishMessageToTopic(bean);
			
		} catch (Exception e) {
			logger.error("publishMessage : Exception while publishing message for order Number : "+bean.getOdrNumber(), e);
		}
				
		logger.trace("[Orde Number] publishMessage : Service processing complete for order Number : "+bean.getOdrNumber());
		
	}

}
