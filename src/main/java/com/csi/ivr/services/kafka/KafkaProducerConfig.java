package com.csi.ivr.services.kafka;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.CommonClientConfigs;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.support.serializer.JsonSerializer;

import com.csi.ivr.services.model.OpenCloseCSIBean;
import com.fd.common.kafka.config.KafkaClusterConfiguration;



@Configuration
public class KafkaProducerConfig {
	
	@Autowired
    private KafkaClusterConfiguration kafkaClusterConfiguration;
	
	@Autowired
	    Environment environment;

	@Bean
    public ProducerFactory<String, OpenCloseCSIBean> producerFactory() 
    {
		
		//Map<String, Object> configProps = kafkaClusterConfiguration.getConfig();
		 Map<String, Object> props = new HashMap<>();
    	 String bootstrapServer= environment.getProperty("spring.kafka.consumer.bootstrap-servers");
         String sslEndpointIdentificationAlgorithm= environment.getProperty("kafka-cluster.ssl.endpoint.identification.algorithm");
         String saslMechanism= environment.getProperty("kafka-cluster.sasl.mechanism");
         String saslJaasConfig= environment.getProperty("kafka-cluster.sasl.jaas.config");
         String requestTimoutMs= environment.getProperty("kafka-cluster.request.timeout.ms");
         String retryBackoffMs= environment.getProperty("kafka-cluster.retry.backoff.ms");
         String securityProtocol= environment.getProperty("kafka-cluster.security.protocol");
         
         props.put(CommonClientConfigs.BOOTSTRAP_SERVERS_CONFIG,bootstrapServer);
         
        
         props.put("ssl.endpoint.identification.algorithm", sslEndpointIdentificationAlgorithm);
         props.put("sasl.mechanism", saslMechanism);
         props.put("sasl.jaas.config", saslJaasConfig);
         props.put("request.timeout.ms", requestTimoutMs);
         props.put("retry.backoff.ms", retryBackoffMs);
         props.put(CommonClientConfigs.SECURITY_PROTOCOL_CONFIG, securityProtocol);
        
         props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);
         props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);
        return new DefaultKafkaProducerFactory<>(props);
    }
 
    @Bean
    public KafkaTemplate<String, OpenCloseCSIBean> kafkaTemplate() 
    {       
        return new KafkaTemplate<>(producerFactory());
    }

}
