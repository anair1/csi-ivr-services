package com.csi.ivr.services.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class OpenCloseCSIBean {
	
	@JsonProperty("odr_number")
	private Number odrNumber;
	@JsonProperty("version")
	private Number version;
	@JsonProperty("leave_open_close_csi")
	private String leaveOpenCloseCSI;
	@JsonProperty("gift_cer")
	private String giftCer;
	@JsonProperty("gc_amt")
	private Double gcAmt;
	@JsonProperty("partial_credit")
	private String partialCredit;
	@JsonProperty("p_credit_amt")
	private Double pCreditAmt;
	@JsonProperty("full_credit")
	private String fullCredit;
	@JsonProperty("f_credit_amt")
	private Double fCreditAmt;
	@JsonProperty("msg_florist")
	private String msgFlorist;
	@JsonProperty("misc")
	private String misc;
	@JsonProperty("redel_apology")
	private String redelApology;
	@JsonProperty("rept_flag")
	private String reptFlag;
	@JsonProperty("caller_code")
	private String callerCode;
	@JsonProperty("contact_type")
	private String contactType;
	@JsonProperty("problem_code")
	private String problemCode;
	@JsonProperty("detail_code")
	private String detailCode;
	@JsonProperty("reason_code")
	private String reasonCode;
	@JsonProperty("res_code")
	private String resCode;
	@JsonProperty("suggest_res_code")
	private String suggestResCode;
	@JsonProperty("status_code")
	private String statusCode;
	@JsonProperty("fault_code")
	private String faultCode;
	@JsonProperty("error_code")
	private String errorCode;
	@JsonProperty("call_back_required_flag")
	private String callBackRequiredFlag;
	@JsonProperty("phone_place_code")
	private String phonePlaceCode;
	@JsonProperty("ti_phone_no")
	private String tiPhoneNo;
	@JsonProperty("call_back_time")
	private String callBackTime;
	@JsonProperty("ti_email")
	private String tiEmail;
	@JsonProperty("start_time")
	private String startTime;
	@JsonProperty("user")
	private String user;
	@JsonProperty("resolution_brand")
	private String resolutionBrand;
	@JsonProperty("managerid_override")
	private String manageridOverride;	
	@JsonProperty("reason_for_override")
	private String reasonForOverride;	
	@JsonProperty("suggested_res_id")
	private Number suggestedResId;		
	
	public Number getOdrNumber() {
		return odrNumber;
	}
	public void setOdrNumber(Number odrNumber) {
		this.odrNumber = odrNumber;
	}
	public Number getVersion() {
		return version;
	}
	public void setVersion(Number version) {
		this.version = version;
	}
	public String getLeaveOpenCloseCSI() {
		return leaveOpenCloseCSI;
	}
	public void setLeaveOpenCloseCSI(String leaveOpenCloseCSI) {
		this.leaveOpenCloseCSI = leaveOpenCloseCSI;
	}
	public String getGiftCer() {
		return giftCer;
	}
	public void setGiftCer(String giftCer) {
		this.giftCer = giftCer;
	}
	public Double getGcAmt() {
		return gcAmt;
	}
	public void setGcAmt(Double gcAmt) {
		this.gcAmt = gcAmt;
	}
	public String getPartialCredit() {
		return partialCredit;
	}
	public void setPartialCredit(String partialCredit) {
		this.partialCredit = partialCredit;
	}
	public Double getpCreditAmt() {
		return pCreditAmt;
	}
	public void setpCreditAmt(Double pCreditAmt) {
		this.pCreditAmt = pCreditAmt;
	}
	public String getFullCredit() {
		return fullCredit;
	}
	public void setFullCredit(String fullCredit) {
		this.fullCredit = fullCredit;
	}
	public Double getfCreditAmt() {
		return fCreditAmt;
	}
	public void setfCreditAmt(Double fCreditAmt) {
		this.fCreditAmt = fCreditAmt;
	}
	public String getMsgFlorist() {
		return msgFlorist;
	}
	public void setMsgFlorist(String msgFlorist) {
		this.msgFlorist = msgFlorist;
	}
	public String getMisc() {
		return misc;
	}
	public void setMisc(String misc) {
		this.misc = misc;
	}
	public String getRedelApology() {
		return redelApology;
	}
	public void setRedelApology(String redelApology) {
		this.redelApology = redelApology;
	}
	public String getReptFlag() {
		return reptFlag;
	}
	public void setReptFlag(String reptFlag) {
		this.reptFlag = reptFlag;
	}
	public String getCallerCode() {
		return callerCode;
	}
	public void setCallerCode(String callerCode) {
		this.callerCode = callerCode;
	}
	public String getContactType() {
		return contactType;
	}
	public void setContactType(String contactType) {
		this.contactType = contactType;
	}
	public String getProblemCode() {
		return problemCode;
	}
	public void setProblemCode(String problemCode) {
		this.problemCode = problemCode;
	}
	public String getDetailCode() {
		return detailCode;
	}
	public void setDetailCode(String detailCode) {
		this.detailCode = detailCode;
	}
	public String getReasonCode() {
		return reasonCode;
	}
	public void setReasonCode(String reasonCode) {
		this.reasonCode = reasonCode;
	}
	public String getResCode() {
		return resCode;
	}
	public void setResCode(String resCode) {
		this.resCode = resCode;
	}
	public String getSuggestResCode() {
		return suggestResCode;
	}
	public void setSuggestResCode(String suggestResCode) {
		this.suggestResCode = suggestResCode;
	}
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public String getFaultCode() {
		return faultCode;
	}
	public void setFaultCode(String faultCode) {
		this.faultCode = faultCode;
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getCallBackRequiredFlag() {
		return callBackRequiredFlag;
	}
	public void setCallBackRequiredFlag(String callBackRequiredFlag) {
		this.callBackRequiredFlag = callBackRequiredFlag;
	}
	public String getPhonePlaceCode() {
		return phonePlaceCode;
	}
	public void setPhonePlaceCode(String phonePlaceCode) {
		this.phonePlaceCode = phonePlaceCode;
	}
	public String getTiPhoneNo() {
		return tiPhoneNo;
	}
	public void setTiPhoneNo(String tiPhoneNo) {
		this.tiPhoneNo = tiPhoneNo;
	}
	public String getCallBackTime() {
		return callBackTime;
	}
	public void setCallBackTime(String callBackTime) {
		this.callBackTime = callBackTime;
	}
	public String getTiEmail() {
		return tiEmail;
	}
	public void setTiEmail(String tiEmail) {
		this.tiEmail = tiEmail;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getResolutionBrand() {
		return resolutionBrand;
	}
	public void setResolutionBrand(String resolutionBrand) {
		this.resolutionBrand = resolutionBrand;
	}
	public String getManageridOverride() {
		return manageridOverride;
	}
	public void setManageridOverride(String manageridOverride) {
		this.manageridOverride = manageridOverride;
	}
	public String getReasonForOverride() {
		return reasonForOverride;
	}
	public void setReasonForOverride(String reasonForOverride) {
		this.reasonForOverride = reasonForOverride;
	}
	public Number getSuggestedResId() {
		return suggestedResId;
	}
	public void setSuggestedResId(Number suggestedResId) {
		this.suggestedResId = suggestedResId;
	}	


}
