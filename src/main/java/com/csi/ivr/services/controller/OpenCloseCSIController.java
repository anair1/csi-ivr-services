package com.csi.ivr.services.controller;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.csi.ivr.services.model.OpenCloseCSIBean;
import com.csi.ivr.services.util.IVRProducerUtil;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/ivr")
@Api(value = "CSIServices", description = " : Has services for Atlas CSI screens")
public class OpenCloseCSIController {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	IVRProducerUtil ivrProducerUtil;
	
	@CrossOrigin(origins = "*", allowCredentials = "true")
	@ApiOperation(value = "Open Close CSI", response = Iterable.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved list"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@PostMapping(value = "/openCloseCSI", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> openCloseCSI(@RequestBody OpenCloseCSIBean csiBean) throws JsonParseException, JsonMappingException, IOException {
		
		
		logger.trace("[Order Number] publishMessage : Incoming request with order number : "
				+ csiBean.getOdrNumber());

		ResponseEntity<Void> responseEntity = null;
		if (null !=csiBean) {			
			ivrProducerUtil.publishMessage(csiBean);
			responseEntity = new ResponseEntity<>(HttpStatus.OK);
		} 
		return responseEntity;
	}
	
	

}
