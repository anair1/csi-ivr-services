package com.csi.ivr.services;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan({ "com.fd","com.csi.ivr.services"})
@SpringBootApplication
public class CsiIvrServicesApplication {

	public static void main(String[] args) {
		SpringApplication.run(CsiIvrServicesApplication.class, args);
	}

}
